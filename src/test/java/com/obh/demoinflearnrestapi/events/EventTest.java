package com.obh.demoinflearnrestapi.events;


import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(JUnitParamsRunner.class)
class EventTest {

    @Test
    public void builder() {
        Event event = Event.builder()
                .name("obh")
                .description("soar platform team")
                .build();
        assertThat(event).isNotNull();
    }

    @Test
    public void javaBean() {
        Event event = new Event();
        String event_name = "event name";
        String event_description = "event description";

        event.setName(event_name);
        event.setDescription(event_description);

        assertThat(event.getName()).isEqualTo(event_name);
        assertThat(event.getDescription()).isEqualTo(event_description);
    }

    @Test
    @Parameters
//            (method = "paramsForFreeTest")
//    ({
//    "0,0,true",
//    "100,0,false",
//    "0,100,false"
//    })
    public void testFree(int basePrice, int maxPrice, boolean isFree) {
        Event event = Event.builder()
                .basePrice(basePrice)
                .maxPrice(maxPrice)
                .build();
        event.update();
        assertThat(event.isFree()).isEqualTo(isFree);
    }

    // parametersFor = 컨벤션. 즉, 해당 문구 뒤에 테스트 메소드 이름을 붙여 놓으면,
    // @Parameters 메소드 인자값으로 자동 매핑
    private Object[] parametersForTestFree() {
        return new Object[] {
                new Object[] {0, 0, true},
                new Object[] {100, 0, false},
                new Object[] {0, 100, false},
                new Object[] {100, 200, false}
        };
    }

    @Test
    public void testOffline() {
        Event event = Event.builder()
                .location("야탑탑")
               .build();

        event.update();

        assertThat(event.isOffline()).isTrue();
    }
}