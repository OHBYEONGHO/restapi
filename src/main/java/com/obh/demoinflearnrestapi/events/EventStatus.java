package com.obh.demoinflearnrestapi.events;

public enum EventStatus {
    DRAFT, PUBLISHED, BEGAN_ENROLLMENT
}
