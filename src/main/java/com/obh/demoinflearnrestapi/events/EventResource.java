package com.obh.demoinflearnrestapi.events;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RepresentationModel;

import java.util.Arrays;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

public class EventResource extends EntityModel<Event> {

//    1) RepresentationModel을 상속하여 작성한 경우
//    // beanSerializer로 직렬화하기 때문에 Event안의 속성값을 Event라는 필드값으로 감싸서 그 안에 넣음.
//    // 그렇게 안하고 1depth에 event의 필드들이 나오게 하고 싶은 경우 사용
//    @JsonUnwrapped
//    private Event event;
//
//    public EventResource(Event event) {
//        this.event = event;
//    }
//
//    public Event getEvent() {
//        return event;
//    }

//    2) RepresentationModel 안에 EntityModel을 상속한 경우
//    getContent 메소드에 이미 JsonUnwrapped 적용되어 있어 바로 사용 가능
    public EventResource(Event event, Link... links) {
        super(event, Arrays.asList(links));
        add(linkTo(EventController.class).slash(event.getId()).withSelfRel());
    }

}
