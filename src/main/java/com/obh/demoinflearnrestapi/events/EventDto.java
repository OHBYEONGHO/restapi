package com.obh.demoinflearnrestapi.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.time.LocalDateTime;

@Builder @Data @NoArgsConstructor @AllArgsConstructor
public class EventDto {
    @NotEmpty
    private String name;                            // 이름
    @NotEmpty
    private String description;                     // 설명
    @NotNull
    private LocalDateTime beginEnrollmentDateTime;  // 이벤트 등록 시작 시간
    @NotNull
    private LocalDateTime closeEnrollmentDateTime;  // 이벤트 등록 마감 시간
    @NotNull
    private LocalDateTime beginEventDateTime;       // 이벤트 시작 일시
    @NotNull
    private LocalDateTime endEventDateTime;         // 이벤트 종료 일시
    private String location;                        // 이벤트 장소(없으면 온라인 모임)
    @Min(0)
    private int basePrice;                          // 기본 등록비(0이면 선착순 등록)
    @Min(0)
    private int maxPrice;                           // 최대 등록비(0이면 무제한 경매)
    @Min(0)
    private int limitOfEnrollment;                  // 인원수 제한

}
