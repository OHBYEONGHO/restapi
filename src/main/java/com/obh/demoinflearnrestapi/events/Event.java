package com.obh.demoinflearnrestapi.events;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
//@EqualsAndHashCode(of={"id", "name"}) // 엔티티 간의 상호참조하는 연관관계일 경우,
// 스택오버플로우 발생 가능하여 특정 필드만 비교하도록 설정 **연관관계 필드는 사용 금지

// @Data 안쓰는 이유는 EqualsAndHashCode 모든 필드로 참조하기에 스택오버플로우 발생 가능
public class Event {

    // 입력 필드
    private String name;                            // 이름
    private String description;                     // 설명
    private LocalDateTime beginEnrollmentDateTime;  // 이벤트 등록 시작 시간
    private LocalDateTime closeEnrollmentDateTime;  // 이벤트 등록 마감 시간
    private LocalDateTime beginEventDateTime;       // 이벤트 시작 일시
    private LocalDateTime endEventDateTime;         // 이벤트 종료 일시
    private String location;                        // 이벤트 장소(없으면 온라인 모임)
    private int basePrice;                          // 기본 등록비(0이면 선착순 등록)
    private int maxPrice;                           // 최대 등록비(0이면 무제한 경매)
    private int limitOfEnrollment;                  // 인원수 제한

    // 출력 필드
    @Id @GeneratedValue
    public Integer id;
    private boolean offline;
    private boolean free;
    @Enumerated(EnumType.STRING)
    private EventStatus eventStatus = EventStatus.DRAFT;

    public void update() {
        if(this.basePrice == 0 && this.maxPrice == 0) {
            this.free = true;
        } else {
            this.free = false;
        }

        if(this.location.isBlank()) {
            this.offline = false;
        } else {
            this.offline = true;
        }
    }
}
